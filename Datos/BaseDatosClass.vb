﻿Option Strict On

Imports System.Data
Imports System.Data.SqlClient
Imports MySql.Data.MySqlClient

Public Class BaseDatosClass

    'Configuración de Usuario.
    Dim SGBD As String = My.Settings.SGBD
    Dim Server As String = My.Settings.Server
    Dim DataBase As String = My.Settings.DataBase
    Dim User As String = My.Settings.User
    Dim Password As String = My.Settings.Password
    Dim Port As String = My.Settings.Port

    'Inicia el string de la cadena de conexión para MSSQL.
    Dim StrConnMSSQL As String = "Data Source=" & Server & "; DataBase=" & DataBase & "; User=" & User & "; Password=" & Password
    'Inicia el string de la cadena de conexión para MySQL.
    Dim StrConnMySQL As String = "Server=" & Server & "; DataBase=" & DataBase & "; User=" & User & "; Password=" & Password & "; Port=" & Port

    'Nombre de la tabla.
    Dim objTabla_ As String

    ''' <summary>
    ''' Indica la tabla con la que va a trabajar la instancia.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property objTabla() As String
        Get
            Return objTabla_
        End Get

        Set(ByVal value As String)
            objTabla_ = value
        End Set
    End Property

    ''' <summary>
    ''' Trae todos los registros de la tabla.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function TraerTodo() As DataTable

        'Comando SQL
        Dim strCommand As String = "SELECT * FROM " & objTabla_

        Return obtenerDataTable(strCommand)

    End Function

    ''' <summary>
    ''' Trae todos los registros de la tabla que cumplen con el fitro.
    ''' </summary>
    ''' <param name="filtro"></param>
    ''' <returns></returns>
    Public Function TraerFiltrado(ByVal filtro As String) As DataTable

        'Comando SQL
        Dim objComando As String = "SELECT * FROM " & objTabla_ & " WHERE " & filtro

        Return obtenerDataTable(objComando)

    End Function

    ''' <summary>
    ''' Inserta un registro en la tabla especificada.
    ''' </summary>
    ''' <param name="comandoSQL"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Function Insertar(ByVal comandoSQL As String) As Integer

        'Comando SQL
        Dim Id As Integer

        If SGBD = "MSSQL" Then
            Using objConexionMSSQL As New SqlConnection(StrConnMSSQL)
                'El select para obtener ultimo Id es diferente para cada motor.
                Dim strCommand As String = "INSERT INTO " & objTabla_ & " " & comandoSQL & "; SELECT SCOPE_IDENTITY()"
                'Crea un objeto MSSQLCommand.
                Dim objCommand As New SqlCommand(strCommand, objConexionMSSQL)

                Try
                    objConexionMSSQL.Open()

                    'Ejecuta el comando strCommand y se obtiene la devolución
                    'de un escalar que representa el Id del último registro insertado.
                    Id = CInt(objCommand.ExecuteScalar())

                Catch ex1 As InvalidOperationException
                    MessageBox.Show(ex1.Message)
                    'Throw New System.Exception(ex1.Message)

                Catch ex2 As SqlException
                    MessageBox.Show(ex2.Message)

                Catch ex3 As Exception
                    MessageBox.Show(ex3.Message)

                End Try

                objConexionMSSQL.Close()
            End Using
        Else
            Using objConexionMySQL As New MySqlConnection(StrConnMySQL)
                'El select para obtener ultimo Id es diferente para cada motor.
                Dim strCommand As String = "INSERT INTO " & objTabla_ & " " & comandoSQL & "; SELECT LAST_INSERT_ID()"
                'Crea un objeto MySQLCommand.
                Dim objCommand As New MySqlCommand(strCommand, objConexionMySQL)

                Try
                    objConexionMySQL.Open()

                    'Ejecuta el comando strCommand y se obtiene la devolución
                    'de un escalar que representa el Id del último registro insertado.
                    Id = CInt(objCommand.ExecuteScalar())

                Catch ex1 As InvalidOperationException
                    MessageBox.Show(ex1.Message)
                    'Throw New System.Exception(ex1.Message)

                Catch ex2 As SqlException
                    MessageBox.Show(ex2.Message)

                Catch ex3 As Exception
                    MessageBox.Show(ex3.Message)

                End Try

                objConexionMySQL.Close()
            End Using
        End If

        Return Id

    End Function

    ''' <summary>
    ''' Elimina el registro de la tabla con el Id indicado.
    ''' </summary>
    ''' <param name="Id"></param>
    ''' <remarks></remarks>
    Public Function Eliminar(ByVal Id As Integer) As Boolean
        'Comando SQL
        Dim strCommand As String = "DELETE FROM " & objTabla_ & " WHERE ID = @Id"
        Dim resultado As Boolean

        If SGBD = "MSSQL" Then
            Using objConexionMSSQL As New SqlConnection(StrConnMSSQL)
                'Crea un objeto MSSQLCommand.
                Dim objCommand As New SqlCommand(strCommand, objConexionMSSQL)
                objConexionMSSQL.Open()

                'Reemplaza el valor de Id en el parametro @ID
                objCommand.Parameters.AddWithValue("@ID", Id)

                Try
                    'Intenta ejecutar el comando SQL.
                    objCommand.ExecuteNonQuery()
                    resultado = True

                Catch ex1 As InvalidOperationException
                    MessageBox.Show(ex1.Message)
                    resultado = False

                Catch ex2 As SqlException
                    MessageBox.Show(ex2.Message)
                    resultado = False

                Catch ex3 As Exception
                    MessageBox.Show(ex3.Message)

                End Try

                objConexionMSSQL.Close()
            End Using
        Else
            Using objConexionMySQL As New MySqlConnection(StrConnMySQL)
                'Crea un objeto MySQLCommand.
                Dim objCommand As New MySqlCommand(strCommand, objConexionMySQL)
                objConexionMySQL.Open()

                'Reemplaza el valor de Id en el parametro @ID
                objCommand.Parameters.AddWithValue("@ID", Id)

                Try
                    'Intenta ejecutar el comando SQL.
                    objCommand.ExecuteNonQuery()
                    resultado = True

                Catch ex1 As InvalidOperationException
                    MessageBox.Show(ex1.Message)
                    resultado = False

                Catch ex2 As SqlException
                    MessageBox.Show(ex2.Message)
                    resultado = False

                Catch ex3 As Exception
                    MessageBox.Show(ex3.Message)

                End Try

                objConexionMySQL.Close()
            End Using
        End If

        Return resultado

    End Function

    ''' <summary>
    ''' Actualiza el registro de la tabla con el Id indicado.
    ''' </summary>
    ''' <param name="comandosql"></param>
    ''' <param name="Id"></param>
    ''' <remarks></remarks>
    Public Function Actualizar(ByVal comandosql As String, ByVal Id As Integer) As Boolean
        'Comando SQL
        Dim strCommand As String = "UPDATE " & objTabla_ & " SET " & comandosql & " WHERE ID = @ID"
        Dim resultado As Boolean

        If SGBD = "MSSQL" Then
            Using objConexionMSSQL As New SqlConnection(StrConnMSSQL)
                'Crea un objeto MSSQLCommand.
                Dim objCommand As New SqlCommand(strCommand, objConexionMSSQL)
                objConexionMSSQL.Open()

                'Reemplaza el valor de Id en el parametro @ID
                objCommand.Parameters.AddWithValue("@ID", Id)

                Try
                    'Intenta ejecutar el comando SQL.
                    objCommand.ExecuteNonQuery()
                    resultado = True

                Catch ex1 As InvalidOperationException
                    MessageBox.Show(ex1.Message)
                    resultado = False

                Catch ex2 As SqlException
                    MessageBox.Show(ex2.Message)
                    resultado = False

                Catch ex3 As Exception
                    MessageBox.Show(ex3.Message)

                End Try

                objConexionMSSQL.Close()
            End Using
        Else
            Using objConexionMySQL As New MySqlConnection(StrConnMySQL)
                'Crea un objeto MySQLCommand.
                Dim objCommand As New MySqlCommand(strCommand, objConexionMySQL)
                objConexionMySQL.Open()

                'Reemplaza el valor de Id en el parametro @ID
                objCommand.Parameters.AddWithValue("@ID", Id)

                Try
                    'Intenta ejecutar el comando SQL.
                    objCommand.ExecuteNonQuery()
                    resultado = True

                Catch ex1 As InvalidOperationException
                    MessageBox.Show(ex1.Message)
                    resultado = False

                Catch ex2 As SqlException
                    MessageBox.Show(ex2.Message)
                    resultado = False

                Catch ex3 As Exception
                    MessageBox.Show(ex3.Message)

                End Try

                objConexionMySQL.Close()
            End Using
        End If

        Actualizar = resultado

    End Function

    Private Function obtenerDataTable(ByVal strCommand As String) As DataTable
        If SGBD = "MSSQL" Then
            Using objConexionMSSQL As New SqlConnection(StrConnMSSQL)
                'Declara el objeto DataAdapter
                Dim objDataAdapter As New SqlDataAdapter(strCommand, objConexionMSSQL)
                'Instancia un objeto DataTable
                Dim objDataTable As New DataTable

                'Intenta llenar el datatable con el DataAdapter.
                Try
                    objDataAdapter.Fill(objDataTable)

                Catch ex1 As InvalidOperationException
                    MessageBox.Show(ex1.Message)
                    'Throw New System.Exception(ex1.Message)

                Catch ex2 As SqlException
                    MessageBox.Show(ex2.Message)

                Catch ex3 As Exception
                    MessageBox.Show(ex3.Message)

                End Try

                Return objDataTable
            End Using
        Else
            Using objConexionMySQL As New MySqlConnection(StrConnMySQL)
                'Crea un objeto MySQLCommand.
                Dim objCommand As New MySqlCommand(strCommand, objConexionMySQL)
                'Declara el objeto DataAdapter
                Dim objDataAdapter As New MySqlDataAdapter(objCommand)
                'Instancia un objeto DataTable
                Dim objDataTable As New DataTable

                Try
                    'Intenta llenar el datatable con el DataAdapter.
                    objDataAdapter.Fill(objDataTable)

                Catch ex1 As InvalidOperationException
                    MessageBox.Show(ex1.Message)
                    'Throw New System.Exception(ex1.Message)

                Catch ex2 As SqlException
                    MessageBox.Show(ex2.Message)

                Catch ex3 As Exception
                    MessageBox.Show(ex3.Message)

                End Try

                Return objDataTable
            End Using
        End If

    End Function

End Class
