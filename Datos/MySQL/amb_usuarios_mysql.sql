/*
 Base de datos Turnos
     Autor: Carlos Murúa.
    Creado: 25/08/2020
Modificado: 25/08/2020
*/

/* Selecciona la base de datos con la cual se van aplicar los siguientes comandos MySQL*/
USE usuarios;

/*-------------PROCEDIMIENTOS DE USUARIOS-----------------------*/
/*Elimina el procedimiento almacenado agregarUsuario si existe, de lo
contrario dará error intentar agregarlo si existiera en la base de datos.*/
DROP PROCEDURE IF EXISTS agregarUsuario;

/*DELIMITER señala que entre los delimitadores // se va enviar ese
bloque de código como contenido del procedimiento almacenado y para
distinguir que no sea ejecutado en su lugar.*/
DELIMITER //
CREATE PROCEDURE agregarUsuario(IN Nombre_ VARCHAR(50), IN Email_ VARCHAR(50), IN Clave_ VARCHAR(50), IN FechaAlta_ DATE, IN FechaLogin_ DATE)
BEGIN
    INSERT INTO Usuarios (Nombre, Email, Clave, FechaAlta, FechaLogin) VALUES (Nombre_, Email_, Clave_, FechaAlta_, FechaLogin_);
    SELECT last_insert_id() AS UltimoID;
END
// 
/*Vuelve a poner como delimitador el punto y coma.*/
DELIMITER ;
/*Entre BEGIN y END es el bloque de código que será guardado como
procedimiento almacenado y no será ejecutado como parte del script. */

/*--------------------------------------*/
DROP PROCEDURE IF EXISTS modificarUsuario;
DELIMITER //
CREATE PROCEDURE modificarUsuario(IN Id INT(5), IN Nombre_ VARCHAR(50), IN Email_ VARCHAR(50), IN Clave_ VARCHAR(50), IN FechaAlta_ DATE, IN FechaLogin_ DATE)
BEGIN
    UPDATE Usuarios SET Nombre = Nombre_, Email = Email_, Clave = Clave_, FechaAlta = FechaAlta_, FechaLogin = FechaLogin_ WHERE Id = Id_;
END
//
DELIMITER ;

/*--------------------------------------*/
DROP PROCEDURE IF EXISTS eliminarUsuario;
DELIMITER //
CREATE PROCEDURE eliminarUsuario(IN Id_ INT(5))
BEGIN
    DELETE FROM Usuarios WHERE Id = Id_;
END
//
DELIMITER ;

/*--------------------------------------*/
DROP PROCEDURE IF EXISTS existeUsuario;
DELIMITER //
CREATE PROCEDURE existeUsuario(In Nombre_ VARCHAR(50))
BEGIN
    SELECT Nombre FROM Usuarios WHERE Nombre = Nombre_;
END
//
DELIMITER ;

/*--------------------------------------*/
DROP PROCEDURE IF EXISTS obtenerUsuario;
DELIMITER //
CREATE PROCEDURE obtenerUsuario(IN Id_ INT(5))
BEGIN
    SELECT  Id, Nombre FROM Usuarios WHERE Id = Id_;
END
//
DELIMITER ;
