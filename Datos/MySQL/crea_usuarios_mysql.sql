/*
 Base de datos usuarios
     Autor: Carlos Murúa.
    Creado: 25/08/2020
Modificado: 25/08/2020
*/

DROP DATABASE IF EXISTS usuarios;

CREATE DATABASE usuarios DEFAULT CHARSET latin1 COLLATE latin1_spanish_ci;

USE usuarios;

CREATE TABLE usuarios (
	Id INT(5) NOT NULL AUTO_INCREMENT,
	Nombre VARCHAR(50) NOT NULL,
	Email VARCHAR(50) NOT NULL,
	Clave VARCHAR(50) NOT NULL,
	FechaAlta DATE NOT NULL,
	FechaLogin DATE NOT NULL,
	PRIMARY KEY (Id)
	) ENGINE=InnoDB;
