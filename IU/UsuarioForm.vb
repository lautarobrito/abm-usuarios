﻿Public Class UsuarioForm

    Public MiUsuario As New UsuarioClass

    Dim operacion_ As String
    Public Property operacion() As String
        Get
            Return operacion_
        End Get

        Set(ByVal value As String)
            operacion_ = value
        End Set
    End Property

    Dim indice_ As Integer
    Public Property indice() As Integer
        Get
            Return indice_
        End Get
        Set(ByVal value As Integer)
            indice_ = value
        End Set
    End Property

    Private Sub Aceptar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Aceptar.Click
        If operacion_ = "Alta" And (IdTextBox.Text = "" Or NombreTextBox.Text = "" Or EmailTextBox.Text = "" Or ClaveTextBox.Text = "" Or FechaAltaTextBox.Text = "" Or FechaLoginTextBox.text = "") Then Exit Sub

        If operacion_ = "Elimina" Or operacion_ = "Modifica" Then
            MiUsuario.Id = CInt(IdTextBox.Text)
        End If

        MiUsuario.Nombre = NombreTextBox.Text
        MiUsuario.Email = EmailTextBox.Text
        MiUsuario.Clave = ClaveTextBox.Text
        MiUsuario.FechaAlta = CDate(FechaAltaTextBox.Text)
        MiUsuario.FechaLogin = CDate(FechaLoginTextBox.Text)

        Select Case operacion_
            Case "Agregar"
                usuario_list.InsertarUsuario(MiUsuario)


            Case "Eliminar"
                usuario_list.EliminarUsuario(MiUsuario)

            Case "Modificar"
                usuario_list.ActualizarUsuario(MiUsuario)
                UsuariosGrid.DataGridView1.RefreshEdit()

        End Select

        Me.Close()

    End Sub

    Private Sub Cancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Cancelar.Click
        Me.Close()
    End Sub

    Private Sub UsuarioForm_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.Text = CStr(operacion_) + " Usuario"
    End Sub

    Private Sub Aceptar_Click_1(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Aceptar.Click

    End Sub

    Private Sub Label1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Label1.Click

    End Sub
End Class

