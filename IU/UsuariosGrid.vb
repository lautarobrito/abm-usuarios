﻿
Public Class UsuariosGrid

    Private Sub UsuariosGrid_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        DataGridView1.DataSource = usuario_list


    End Sub

    Private Sub AgregarToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarToolStripButton.Click

        UsuarioForm.operacion = "Agregar"
        UsuarioForm.Show()

    End Sub

    Private Sub EliminarToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EliminarToolStripButton.Click
        'Si la lista tiene cero elementos no hay nada para eliminar.
        If usuario_list.Count = 0 Then Exit Sub

        'Se asigna el valor "Elimina" a la propiedad operación.
        UsuarioForm.operacion = "Eliminar"
        'Ejecuta el método llenarform para actalizar los controles.
        LlenarUsuarioForm()
        'Muestra el formulario UsuarioForm.
        UsuarioForm.Show()

    End Sub

    Private Sub ModificarToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ModificarToolStripButton.Click
        'Si la lista tiene cero elementos no hay nada para modificar.
        If usuario_list.Count = 0 Then Exit Sub

        'Se asigna el valor "Modifica" a la propiedad operación.
        UsuarioForm.operacion = "Modificar"
        'Ejecuta el método llenarUsuarioform
        llenarUsuarioForm()
        'Muestra el formulario UsuarioForm.
        UsuarioForm.Show()

    End Sub

    Private Sub SalirToolStripButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripButton.Click
        End
    End Sub

    Private Sub llenarUsuarioForm()

        'Con esto obtenemos el índice de la fila actual o seleccionada.
        Dim fila As Integer = DataGridView1.CurrentRow.Index

        UsuarioForm.IdTextBox.Text = usuario_list.Item(fila).Id.ToString
        UsuarioForm.NombreTextBox.Text = usuario_list.Item(fila).Nombre.ToString
        UsuarioForm.EmailTextBox.Text = usuario_list.Item(fila).Email.ToString
        UsuarioForm.ClaveTextBox.Text = usuario_list.Item(fila).Clave.ToString
        UsuarioForm.FechaAltaTextBox.Text = Format(usuario_list.Item(fila).FechaAlta, "dd-MM-yyyy")
        UsuarioForm.FechaLoginTextBox.Text = Format(usuario_list.Item(fila).FechaLogin, "dd-MM-yyyy")

    End Sub

    Private Sub ToolStripButton1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles AgregarToolStripButton.Click

    End Sub

    Private Sub ToolStrip1_ItemClicked(ByVal sender As System.Object, ByVal e As System.Windows.Forms.ToolStripItemClickedEventArgs) Handles ToolStrip1.ItemClicked

    End Sub

    Private Sub DataGridView1_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick

    End Sub
End Class

