﻿Public Class UsuarioClass
    'Las variables guardan el estado del la propiedad.
    Dim Id_ As Integer
    'Código que permite Leer y Escribir en la variable.
    Public Property Id() As Integer
        Get
            'Devuelve el valor de la variable.
            Return Id_
        End Get
        Set(ByVal value As Integer)
            'Escribe  un valor en la variable.
            Id_ = value
        End Set
    End Property

    Dim Nombre_ As String

    Public Property Nombre() As String
        Get
            Return Nombre_
        End Get
        Set(ByVal value As String)
            Nombre_ = value
        End Set
    End Property

    Dim Email_ As String

    Public Property Email() As String
        Get
            Return Email_

        End Get
        Set(ByVal value As String)
            Email_ = value
        End Set
    End Property

    Dim Clave_ As String

    Public Property Clave() As String
        Get
            Return Clave_
        End Get
        Set(ByVal value As String)
            Clave_ = value
        End Set
    End Property

    Dim FechaAlta_ As Date

    Public Property FechaAlta() As Date
        Get
            Return FechaAlta_
        End Get
        Set(ByVal value As Date)
            FechaAlta_ = value
        End Set
    End Property
    Dim FechaLogin_ As Date
    Public Property FechaLogin() As Date
        Get
            Return FechaLogin_
        End Get
        Set(ByVal value As Date)
            FechaLogin_ = value
        End Set
    End Property
End Class


