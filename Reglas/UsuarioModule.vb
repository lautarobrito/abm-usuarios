﻿Module UsuarioModule
    'Variable del tipo PaisesCollection para contener la lista de Países.
    'Es pública y se declara en el módulo principal para que esté visible durante
    'la ejecución de la aplicación.

    Public usuario_list As UsuariosCollection


    Sub main()
        'Creó el objeto usuario_list o una nueva instancia de AlumnoCollection
        usuario_list = New UsuariosCollection


        'Ejecuta o levanta el formulario PaisesGrilla.
        Application.Run(UsuariosGrid)
    End Sub
End Module
