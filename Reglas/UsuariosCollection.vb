﻿Imports System.ComponentModel
Imports System.Text

Public Class UsuariosCollection

    Inherits BindingList(Of UsuarioClass)

    Protected Overrides Sub OnAddingNew(ByVal e As System.ComponentModel.AddingNewEventArgs)
        MyBase.OnAddingNew(e)

        e.NewObject = New UsuarioClass
    End Sub

    Protected Overrides ReadOnly Property SupportsSearchingCore() As Boolean
        Get
            Return True
        End Get
    End Property

    Protected Overrides Function FindCore(ByVal prop As PropertyDescriptor, ByVal key As Object) As Integer
        For Each usuario In Me
            If prop.GetValue(usuario).Equals(key) Then
                Return Me.IndexOf(usuario)
            End If
        Next

        Return -1
    End Function

    Public Sub New()
        Me.TraerUsuarios()
    End Sub

    Public Function TraerUsuarios() As UsuariosCollection
        'Si tiene elementos la limpiamos.
        If Me.Items.Count > 0 Then Me.ClearItems()

        Dim objBaseDatos As New BaseDatosClass
        Dim MiDataTable As New DataTable
        Dim MiUsuario As UsuarioClass

        objBaseDatos.objTabla = "Usuarios"
        MiDataTable = objBaseDatos.TraerTodo

        For Each dr As DataRow In MiDataTable.Rows
            MiUsuario = New UsuarioClass

            MiUsuario.Id = CInt(dr("Id").ToString)
            MiUsuario.Nombre = dr("Nombre").ToString
            MiUsuario.Email = dr("Email").ToString
            MiUsuario.Clave = dr("Clave").ToString
            MiUsuario.FechaAlta = CDate(Format(CDate(dr("FechaAlta").ToString), "dd-MM-yyyy"))
            MiUsuario.FechaLogin = CDate(Format(CDate(dr("FechaLogin").ToString), "dd-MM-yyyy"))

            Me.Add(MiUsuario)
        Next

        Return Me

    End Function

    Public Sub InsertarUsuario(ByVal MiUsuario As UsuarioClass)
        'Instancio el el Objeto BaseDatosClass para acceder al la base Usuarios.
        Dim objBaseDatos As New BaseDatosClass
        objBaseDatos.objTabla = "usuarios"

        Dim vSQL As New StringBuilder
        Dim vResultado As Boolean = False

        vSQL.Append("(Nombre")
        vSQL.Append(",Email")
        vSQL.Append(",Clave")
        vSQL.Append(",FechaAlta")
        vSQL.Append(",FechaLogin)")

        vSQL.Append(" VALUES ")

        vSQL.Append("('" & MiUsuario.Nombre & "'")
        vSQL.Append(",'" & MiUsuario.Email & "'")
        vSQL.Append(",'" & MiUsuario.Clave & "'")
        vSQL.Append(",'" & Format(MiUsuario.FechaAlta, "yyyy-MM-dd") & "'")
        vSQL.Append(",'" & Format(MiUsuario.FechaLogin, "yyyy-MM-dd") & "')")

        'Agrego MiUsuario en la tabla Usuarios.
        MiUsuario.Id = objBaseDatos.Insertar(vSQL.ToString)

        'Evalúa el resultado del comando SQL.
        If MiUsuario.Id = 0 Then
            MsgBox("No fue posible agregar el Usuario ")
            Exit Sub
        End If

        'Agrego MiUsuario en la colección actual.
        Me.Add(MiUsuario)

    End Sub

    Public Sub EliminarUsuario(ByVal MiUsuario As UsuarioClass)
        'Instancio el el Objeto BaseDatosClass para acceder al la base Usuarios.
        Dim objBaseDatos As New BaseDatosClass
        objBaseDatos.objTabla = "Usuarios"

        'Ejecuta el método base eliminar.
        Dim resultado As Boolean
        resultado = objBaseDatos.Eliminar(MiUsuario.Id)

        If Not resultado Then
            MessageBox.Show("No fue posible eliminar el Usuario.")
            Exit Sub
        End If

        'Creates a new collection and assign it the properties for modulo.
        Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(MiUsuario)

        'Sets an PropertyDescriptor to the specific property Id.
        Dim myProperty As PropertyDescriptor = properties.Find("Id", False)

        'Remueve o elimina el usuario de la lista.
        Me.RemoveAt(Me.FindCore(myProperty, MiUsuario.Id))

    End Sub

    Public Sub ActualizarUsuario(ByVal MiUsuario As UsuarioClass)
        'Instancio el el Objeto BaseDatosClass para acceder al la base Usuarios.
        Dim objBaseDatos As New BaseDatosClass
        objBaseDatos.objTabla = "Usuarios"

        Dim vSQL As New StringBuilder
        Dim vResultado As Boolean = False

        vSQL.Append("Nombre='" & MiUsuario.Nombre.ToString & "'")
        vSQL.Append(",Email='" & MiUsuario.Email.ToString & "'")
        vSQL.Append(",Clave='" & MiUsuario.Clave.ToString & "'")
        vSQL.Append(",FechaAlta='" & Format(MiUsuario.FechaAlta.ToString, "yyyy-MM-dd") & "'")
        vSQL.Append(",FechaLogin='" & Format(MiUsuario.FechaLogin.ToString, "yyyy-MM-dd") & "'")

        'Actualizo la tabla Usuarios con el Id.
        Dim resultado As Boolean
        'El método actualizar es una función que devuelve True o False
        'Según como haya resultado la operación sobre la tabla SQL.
        resultado = objBaseDatos.Actualizar(vSQL.ToString, MiUsuario.Id)

        If Not resultado Then
            MessageBox.Show("No fue posible modificar el Usuario.")
        End If

        'El código a continuación sirve para localizar el ítem en la lista
        'en este caso un Usuario.
        ' Creates a new collection and assign it the properties for modulo.
        Dim properties As PropertyDescriptorCollection = TypeDescriptor.GetProperties(MiUsuario)

        'Sets an PropertyDescriptor to the specific property Id.
        Dim myProperty As PropertyDescriptor = properties.Find("Id", False)
        Me.Items.Item(Me.FindCore(myProperty, MiUsuario.Id)) = MiUsuario

    End Sub

End Class

